import React, { Component } from "react";
import HeadBoard from "./components/HeadBoard/HeadBoard";
import Board from "./components/Board/Board";
import './styles/style.css'

class App extends Component {
  constructor() {
    super();

    this.state = {
      gameStatus: "waiting",
      triesCount: 0,
      openCells: 0,
      items: 1,
      rows: 6,
      columns: 6
    };

    this.baseState = this.state;
  }

  componentDidUpdate(nextProps, nextState) {
    if (this.state.gameStatus === "running") {
      this.checkForWinner();
    }
  }
  

  checkForWinner = () => {
    if (this.state.items + this.state.openCells >= this.state.rows * this.state.columns) {
      this.setState({
        gameStatus: "winner"
      }, alert("ты победил"))
    }
  }

  componentWillMount() {
    this.intervals = [];
  }

  setInterval = (fn, t) => {
    this.intervals.push(setInterval(fn, t));
  };

  reset = () => {
    this.intervals.map(clearInterval);
    this.setState(Object.assign({}, this.baseState), () => {
      this.intervals = [];
    });
  };

  endGame = () => {
    this.setState({
      gameStatus: "ended"
      
    }, alert("конец игры, предмет найден"));
  };

  handleCellClick = () => {
    if (this.state.openCells === 0 && this.state.gameStatus !== "running") {
      this.setState(
        {
          gameStatus: "running"
        }
      );
    }
    this.setState(prevState => {
      return { openCells: prevState.openCells + 1,
      triesCount: prevState.triesCount+ 1 };
    });
  };

  render() {
    return (
      <div className="minesweeper">
        <HeadBoard
          Used={this.state.triesCount}
          reset={this.reset}
          status={this.state.gameStatus}
        />
        <Board
          openCells={this.state.openCells}
          items={this.state.items}
          rows={this.state.rows}
          columns={this.state.columns}
          endGame={this.endGame}
          status={this.state.gameStatus}
          onCellClick={this.handleCellClick}
        />
      </div>
    );
  }
}

export default App;