import React from 'react'
import PropTypes from "prop-types";

const HeadBoard =props =>{
    
  let status =
    props.status === "running" || props.status === "waiting" ? (
        <i className="icon ion-sad-outline" />
    ) : (
      
      <i className="icon ion-happy-outline" />
    );
  return (
    <div className="board-head">
      <div className="try-count"> Tries {props.Used}</div>
      <button className="reset" onClick={props.reset}>
        {status}Reset
      </button>
      
    </div>
  );
};

HeadBoard.propTypes = {
  
  Used: PropTypes.number.isRequired
};

export default HeadBoard;