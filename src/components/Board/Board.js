import React, { Component } from "react";
import Row from "../Row/Row";

class Board extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rows: this.createBoard(props)
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.openCells > nextProps.openCells ||
      this.props.columns !== nextProps.columns
    ) {
      this.setState({
        rows: this.createBoard(nextProps)
      });
    }
  }

  createBoard = props => {
    
    let board = [];
    for (let i = 0; i < props.rows; i++) {
      board.push([]);
      for (let j = 0; j < props.columns; j++) {
        board[i].push({
          x: j,
          y: i,
          isOpen: false,
          hasItem: false,
          
        });
      }
    }
    
    for (let i = 0; i < props.items; i++) {
      let randomRow = Math.floor(Math.random() * props.rows);
      let randomCol = Math.floor(Math.random() * props.columns);

      let cell = board[randomRow][randomCol];

      if (cell.hasItem) {
        
        i--;
      } else {
        cell.hasItem = true;
      }
    }
    return board;
  };

  
  open = cell => {
    if (this.props.status === "ended") {
      return;
    }
    else{
      let rows = this.state.rows;

      let current = rows[cell.y][cell.x];

      if (current.hasItem && this.props.openCells === 0) {
        alert("ты победил с первого раза");
        
        let newRows = this.createBoard(this.props);
        this.setState({ rows: newRows }, () => {
          this.open(cell);
        });

    } else if (!cell.hasTried && !current.isOpen) {
      this.props.onCellClick();

      current.isOpen = true;
      if (current.hasItem && this.props.openCells !== 0) {
        this.props.endGame();
      }

    }
  }
  };

  
  

  
  render() {
    let rows = this.state.rows.map((cells, index) => (
      <Row
        cells={cells}
        open={this.open}
        key={index}
      />
    ));
    return <div className="board">{rows}</div>;
  }
}

export default Board;