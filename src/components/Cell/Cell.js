import React from "react";

const Cell = props => {
  let cell = () => {
    if (props.data.isOpen) {
      if (props.data.hasItem) {
        return (
          <div
            className="cell open"
            onClick={(e) =>  {e.preventDefault(props.open(props.data))}}
          >
            <span><i className="icon ion-android-radio-button-on"></i></span>
          </div>
        );
      }  else {
        return (
          <div
            className="cell open"
            
            onClick={() => props.open(props.data)}
          >
         
          </div>
        );
      }
    
    } else {
      return (
        <div
          className="cell"
         
          onClick={() => props.open(props.data)}
        />
      );
    }
  };
  return cell();
};

export default Cell;